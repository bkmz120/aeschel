'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var streamqueue  = require('streamqueue');
var autoprefixer = require('gulp-autoprefixer');
var uglifycss = require('gulp-uglifycss');
var minify = require('gulp-minify');
 
gulp.task('sass', function () {
	return streamqueue({ objectMode: true },
		gulp.src('./web/css/source/index.scss').pipe(sass({outputStyle:"expanded"}).on('error', sass.logError))
	)		
    .pipe(concat('build.css'))
    .pipe(autoprefixer({
	    browsers: ['last 2 versions'],
	    cascade: false
	}))    
    .pipe(gulp.dest('./web/css'));
});


gulp.task('js-libs',function() {
  return streamqueue({ objectMode: true },
  		gulp.src('./web/js/libs/jquery.js'),  		
  		gulp.src('./web/js/libs/slick.min.js'),
      gulp.src('./web/js/libs/jquery.inputmask.bundle.min.js'),
      gulp.src('./web/js/libs/routie.min.js')
    )
  	.pipe(concat('libs.js'))
    .pipe(gulp.dest('./web/js/dist'));
});

gulp.task('js-source-common',function() {
  return streamqueue({ objectMode: true },
  		gulp.src('./web/js/source/common/fixed-head.js'),
      gulp.src('./web/js/source/common/custom-form.js'),
      gulp.src('./web/js/source/common/main-form.js'),
      gulp.src('./web/js/source/common/order-popup.js')
    )
  	.pipe(concat('common.js'))
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: [],
        ignoreFiles: []
    }))
    .pipe(gulp.dest('./web/js/dist'));
});

gulp.task('js-source-main',function() {
  return streamqueue({ objectMode: true },
      gulp.src('./web/js/source/main/main-page.js'),
      gulp.src('./web/js/source/main/work-viewer.js')
    )
    .pipe(concat('main.js'))
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: [],
        ignoreFiles: []
    }))
    .pipe(gulp.dest('./web/js/dist'));
});

gulp.task('js-source-napravleniya',function() {
  return streamqueue({ objectMode: true },
      gulp.src('./web/js/source/napravleniya/napravleniya-menu.js')
    )
    .pipe(concat('napravleniya.js'))
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: [],
        ignoreFiles: []
    }))
    .pipe(gulp.dest('./web/js/dist'));
});


gulp.task('js-source-equipment',function() {
  return streamqueue({ objectMode: true },
      gulp.src('./web/js/source/equipments/equipment-popup.js'),
      gulp.src('./web/js/source/equipments/equipment-page.js')
    )
    .pipe(concat('equipments.js'))
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: [],
        ignoreFiles: []
    }))
    .pipe(gulp.dest('./web/js/dist'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch(['./web/css/source/**/*.scss','./web/css/source/**/*.css'], ['sass']);
});

gulp.task('default',function(){
  gulp.watch(['./web/css/source/**/*.scss','./web/css/source/**/*.css'], function(){
    setTimeout(function(){gulp.start('sass')},800);
  });
  gulp.watch('./web/js/source/**/*.js', ['js-source-common','js-source-main','js-source-napravleniya','js-source-equipment']);
  gulp.watch('./web/js/libs/**/*.js', ['js-libs']);  
});


gulp.task('build', ['js-libs','js-source-common','js-source-main','js-source-napravleniya','js-source-equipment', 'sass']);