<?php

namespace app\models;

use Yii;
use janisto\ycm\behaviors\FileBehavior;

/**
 * This is the model class for table "portfolio".
 *
 * @property int $id
 * @property string $name
 * @property string $photo
 * @property string $location
 * @property string $text
 */
class Portfolio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => FileBehavior::className(),
                'folderName' => 'portfolio',
            ]
        ];
    }

    public $adminNames = ['Выполненные работы'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'portfolio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'location', 'text'], 'required'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 511],
            [['photo'], 'image', 'maxSize' => 1024 * 1024 * 1, 'maxWidth' => 2560, 'maxHeight' => 2560],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Заголовок',
            'photo' => 'Изображение',
            'location' => 'Место раположения',
            'text' => 'Описание',
        ];
    }

    /**
     * Config for attribute widgets (ycm)
     *
     * @return array
     */
    public function attributeWidgets()
    {
        /**
         * You should configure attribute widget as: ['field', 'widget'].
         *
         * If you need to pass additional configuration to the widget, add them as: ['field', 'widget', 'key' => 'value'].
         * The first two items are sliced from the array, and then it's passed to the widget as: ['key' => 'value'].
         *
         * There are couple of exceptions to this slicing:
         * If you use 'hint', 'hintOptions', 'label' or 'labelOptions' the widget will render hint and/or label and
         * then it will unset those keys and pass the array to the widget.
         *
         * If you use 'widget', you must also set 'widgetClass'.
         *
         * Check each widget to see what options you can override/set.
         */

        return [
            ['photo', 'image'],
            ['text','wysiwyg']
        ];
    }

    /**
     * Grid view columns for ActiveDataProvider (ycm)
     *
     * @return array
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#column-classes
     * @see http://www.yiiframework.com/doc-2.0/guide-output-formatter.html#other-formatters
     * @see http://www.bsourcecode.com/yiiframework2/gridview-in-yiiframework-2-0/#GridView-Column-Content-Options
     */
    public function gridViewColumns()
    {
        return [
            'name',
            'location',
            [
                'attribute' => 'photo',
                'format' => ['image', ['width' => '100', 'height' => '100']],
                'value' => function($model) {
                    return $model->getFileUrl('photo');
                },
                'contentOptions' => ['class' => 'image-class'],
            ],
        ];
    }

    /**
     * Grid view sort for ActiveDataProvider (ycm)
     *
     * @return array
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#sorting-data
     */
    public function gridViewSort()
    {
        return [
            'defaultOrder' => [
                'id' => SORT_DESC,
            ]
        ];
    }
}
