<?php

namespace app\models;

use Yii;
use janisto\ycm\behaviors\FileBehavior;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $manuf_id
 * @property int $subcat_id
 * @property string $name
 * @property string $img
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => FileBehavior::className(),
                'folderName' => 'product',
            ]
        ];
    }

    public $adminNames = ['Оборудование'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manuf_id', 'subcat_id', 'name'], 'required'],
            [['manuf_id', 'subcat_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['img'], 'image', 'maxSize' => 1024 * 1024 * 5, 'maxWidth' => 2560, 'maxHeight' => 2560],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manuf_id' => 'Производитель',
            'subcat_id' => 'Подкатегория',
            'name' => 'Название',
            'img' => 'Изображение',
        ];
    }

    public static function getImgUrl($imgName) {
        return "/web/uploads/product/img/".$imgName;
    }

    /**
     * Config for attribute widgets (ycm)
     *
     * @return array
     */
    public function attributeWidgets()
    {
        /**
         * You should configure attribute widget as: ['field', 'widget'].
         *
         * If you need to pass additional configuration to the widget, add them as: ['field', 'widget', 'key' => 'value'].
         * The first two items are sliced from the array, and then it's passed to the widget as: ['key' => 'value'].
         *
         * There are couple of exceptions to this slicing:
         * If you use 'hint', 'hintOptions', 'label' or 'labelOptions' the widget will render hint and/or label and
         * then it will unset those keys and pass the array to the widget.
         *
         * If you use 'widget', you must also set 'widgetClass'.
         *
         * Check each widget to see what options you can override/set.
         */

        return [
            ['manuf_id', 'select'],
            ['subcat_id', 'select'],
            ['img', 'image'],
        ];
    }

    public static function manuf_idChoices()
    {
        $manufs = ProductManuf::find()->asArray()->all();
        $manufsChoice = [];
        foreach ($manufs as $manuf) {
            $manufsChoice[$manuf['id']]=$manuf['name'];
        }
        return $manufsChoice;
    }

    public static function subcat_idChoices()
    {
        $subcats = ProductSubcat::find()->asArray()->all();
        $subcatsChoice = [];
        foreach ($subcats as $subcat) {
            $subcatsChoice[$subcat['id']]=$subcat['name'];
        }
        return $subcatsChoice;
    }

    /**
     * Grid view columns for ActiveDataProvider (ycm)
     *
     * @return array
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#column-classes
     * @see http://www.yiiframework.com/doc-2.0/guide-output-formatter.html#other-formatters
     * @see http://www.bsourcecode.com/yiiframework2/gridview-in-yiiframework-2-0/#GridView-Column-Content-Options
     */
    public function gridViewColumns()
    {
        return [
            name,
            [
                'label'=>'Производитель',
                'attribute' => 'manuf_id',
                'value' => function ($model) {
                    if (isset($this->manuf_idChoices()[$model->manuf_id])) {
                        return $this->manuf_idChoices()[$model->manuf_id];
                    }
                    return null;
                },
            ],
            [
                'label'=>'Подкатегория',
                'attribute' => 'subcat_id',
                'value' => function ($model) {
                    if (isset($this->subcat_idChoices()[$model->subcat_id])) {
                        return $this->subcat_idChoices()[$model->subcat_id];
                    }
                    return null;
                },
            ],
            [
                'attribute' => 'img',
                'format' => ['image', ['width' => '100', 'height' => '50']],
                'value' => function($model) {
                    return $model->getFileUrl('img');
                },
                'contentOptions' => ['class' => 'image-class'],
            ],
        ];
    }

    /**
     * Grid view sort for ActiveDataProvider (ycm)
     *
     * @return array
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#sorting-data
     */
    public function gridViewSort()
    {
        return [
            'defaultOrder' => [
                'name' => SORT_ASC,
            ]
        ];
    }
}
