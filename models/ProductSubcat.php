<?php

namespace app\models;

use Yii;
use janisto\ycm\behaviors\FileBehavior;


/**
 * This is the model class for table "product_subcat".
 *
 * @property int $id
 * @property int $root_catagory
 * @property string $name
 */
class ProductSubcat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => FileBehavior::className(),
                'folderName' => 'product_subcat',
            ]
        ];
    }

    public $adminNames = ['Подкатегории оборудования'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_subcat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['root_catagory', 'name'], 'required'],
            [['root_catagory'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_catagory' => 'Основная категория',
            'name' => 'Название подкатегории',
        ];
    }

    /**
     * Config for attribute widgets (ycm)
     *
     * @return array
     */
    public function attributeWidgets()
    {
        /**
         * You should configure attribute widget as: ['field', 'widget'].
         *
         * If you need to pass additional configuration to the widget, add them as: ['field', 'widget', 'key' => 'value'].
         * The first two items are sliced from the array, and then it's passed to the widget as: ['key' => 'value'].
         *
         * There are couple of exceptions to this slicing:
         * If you use 'hint', 'hintOptions', 'label' or 'labelOptions' the widget will render hint and/or label and
         * then it will unset those keys and pass the array to the widget.
         *
         * If you use 'widget', you must also set 'widgetClass'.
         *
         * Check each widget to see what options you can override/set.
         */

        return [
            ['root_catagory', 'select'],
        ];
    }

    public static function root_catagoryChoices()
    {
        return ProductRootcat::getNames();
    }

    /**
     * Grid view columns for ActiveDataProvider (ycm)
     *
     * @return array
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#column-classes
     * @see http://www.yiiframework.com/doc-2.0/guide-output-formatter.html#other-formatters
     * @see http://www.bsourcecode.com/yiiframework2/gridview-in-yiiframework-2-0/#GridView-Column-Content-Options
     */
    public function gridViewColumns()
    {
        return [
            name,
            //'field_select',
            [
                'label'=>'Категория',
                'attribute' => 'root_catagory',
                'value' => function ($model) {
                    if (isset($this->root_catagoryChoices()[$model->root_catagory])) {
                        return $this->root_catagoryChoices()[$model->root_catagory];
                    }
                    return null;
                },
            ],
        ];
    }

    /**
     * Grid view sort for ActiveDataProvider (ycm)
     *
     * @return array
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#sorting-data
     */
    public function gridViewSort()
    {
        return [
            'defaultOrder' => [
                'root_catagory' => SORT_DESC,
            ]
        ];
    }
}
