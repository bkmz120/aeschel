<?php
use app\assets\NapravleniyaAsset;
use yii\helpers\Url;

NapravleniyaAsset::register($this);
?>

<?php $this->beginContent('@app/views/layouts/base.php'); ?>
<div class="text-page">
	<div class="text-page_menu">
		<?php echo $this->render('@app/views/main/napravleniya/components/menu',['activeItem'=>$this->params['catagoryView'],'activeSubItem'=>$this->params['subCatagoryView']]); ?>
	</div>	
	<div class="text-page_content">
		<?php echo $content; ?>		
	</div>
	<div class="text-page_bottom"></div>
</div>
<?php $this->endContent(); ?>