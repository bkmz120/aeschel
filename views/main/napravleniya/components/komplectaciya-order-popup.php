<div class="equip-popup">
	<div class="equip-popup__close" onclick="window.equipmentPopup.close()"></div>
	<div class="equip-popup__inputs">
		<div class="equip-popup__prod">
			<div class="equip-popup__prod-img-wr">	
				<img src="" class="equip-popup__prod-img">
			</div>			
			<div class="equip-popup__prod-name productName">Тепловычислитель ТСР-025</div>
		</div>
		<div class="equip-popup__inp equip-popup__inp_title">
			<div class="equip-popup__title">Задать вопрос/заказать:</div>
		</div>
		<div class="equip-popup__inp formWrapName">
			<div class="equip-popup__inp-label">Имя <span class="equip-popup__inp-req formError">обязательное поле</span></div>
			<input type="text" class="equip-popup__inp-val formInput">
		</div>
		<div class="equip-popup__inp formWrapPhone">
			<div class="equip-popup__inp-label">Телефон <span class="equip-popup__inp-req formError">обязательное поле</span></div>
			<input type="text" class="equip-popup__inp-val formInput">
		</div>
		<div class="equip-popup__inp formWrapEmail">
			<div class="equip-popup__inp-label">Email <span class="equip-popup__inp-req formError">обязательное поле</span></div>
			<input type="text" class="equip-popup__inp-val formInput">
		</div>
		<div class="equip-popup__inp formWrapText">
			<div class="equip-popup__inp-label">Сообщение <span class="equip-popup__inp-req formError">обязательное поле</span></div>
			<textarea type="text" class="equip-popup__text-val formInput"></textarea>
		</div>
		<div class="equip-popup__inp equip-popup__inp_action formWrapConfirm">
			<div class="equip-popup__confirmCehck-wrap ">
				<input type="checkbox" id="equip-popup__confirmCehck" class="equip-popup__confirmCehck formInput">
				<label for="equip-popup__confirmCehck" class="equip-popup__confirmCehck-label">Даю согласие на обработку <a href="/" class="equip-popup__confirmCehck-link formShowFullConfirm">пересональных данных</a></label>
			</div>
			<div class="equip-popup__full-confirm formFullConfirm">
				<div class="equip-popup__full-confirm-close formCloseFullConfirm"></div>
				<input type="checkbox" id="equip-popup__full-confirm-check" class="equip-popup__full-confirm-check formFullConfirmCheck">
				<label for="equip-popup__full-confirm-check" class="equip-popup__full-confirm-label">Я выражаю свое согласие на осуществление обработки (сбора, систематизации, накопления, хранения, уточнения, обновления, изменения, использования, обезличивания, блокирования и уничтожения) в том числе автоматизированной, моих персональных данных, указанных на сайте, в соответствии с требованиями Федерального закона от 27.07.2006 года № 152-ФЗ «О персональных данных».</label>
			</div>
			<div class="equip-popup__sendBtn formSendBtn">Отправить</div>
			<div class="equip-popup__inp-req equip-popup__inp-req_confirm">Необходимо дать согласие на обработку персональных данных</div>
		</div>
	</div>
	<div class="equip-popup__sending">
		<img src="/img/loading.gif" class="equip-popup__sending-img">
	</div>
	<div class="equip-popup__success">
		<div class="equip-popup__success-text">Сообщение отправлено!</div>
		<div class="equip-popup__closeBtn" onclick="window.equipmentPopup.close()">Закрыть</div>
	</div>
	<div class="equip-popup__fail">
		<div class="equip-popup__fail-text">Произошла ошибка. Сообщение не доставлено.</div>
		<div class="equip-popup__closeBtn" onclick="window.equipmentPopup.close()">Закрыть</div>
	</div>		
</div>
<div class="equip-popup__bg" onclick="window.equipmentPopup.close()"></div>