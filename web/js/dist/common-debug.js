var fixedHead = false;

$(window).scroll(function() {
    var pos = $(this).scrollTop();   

    if (pos>89) {
    	if (!fixedHead)
    	{
    		$('.header-wrap').addClass('header-wrap__fixed');
        	$('.main-slider').css('margin-top',89);
        	fixedHead = true;
    	}  	
    }
    else{
    	if (fixedHead)
    	{
	        $('.header-wrap').removeClass('header-wrap__fixed');
	        $('.main-slider').css('margin-top',0);
	        fixedHead = false;
	    }
    }  
});
(function($) {
  //Класс для валидации и отправки формы 
  function CustomForm(settings) {
    var formEl;
    var inputNameEl,inputEmailEl,inputPhoneEl,inputConfirmEl,inputTextEl,productNameEl;
    var wrapNameEl,wrapEmailEl,wrapPhoneEl,wrapConfirmEl,wrapTextEl;
    var sendBtnEl;
    var fullConfirmEL,showFullConfirmEl,fullConfirmCheckEl,closeFullConfirmEl;

    var validateStatus;

    this.send = send;
    this.clear = clear;

    init();

    function init() {     

      formEl = $(settings.formSelector);      

      wrapNameEl = $(settings.formSelector + ' .formWrapName');
      wrapEmailEl = $(settings.formSelector + ' .formWrapEmail');
      wrapPhoneEl = $(settings.formSelector + ' .formWrapPhone');
      wrapConfirmEl = $(settings.formSelector + ' .formWrapConfirm');
      wrapTextEl = $(settings.formSelector + ' .formWrapText'); 

      productNameEl = $(settings.formSelector + ' .productName');
      inputNameEl = $(settings.formSelector + ' .formWrapName .formInput');
      inputEmailEl = $(settings.formSelector + ' .formWrapEmail .formInput');
      inputPhoneEl = $(settings.formSelector + ' .formWrapPhone .formInput');
      inputConfirmEl = $(settings.formSelector + ' .formWrapConfirm .formInput');
      inputTextEl = $(settings.formSelector + ' .formWrapText .formInput');

      sendBtnEl = $(settings.formSelector + ' .formSendBtn');
      sendBtnEl.on("click",validate);
      inputPhoneEl.inputmask({"mask": "+7 (999) 999-9999"});

      fullConfirmEL = $(settings.formSelector + ' .formFullConfirm');
      showFullConfirmEl = $(settings.formSelector + ' .formShowFullConfirm');
      fullConfirmCheckEl = $(settings.formSelector + ' .formFullConfirmCheck');
      closeFullConfirmEl = $(settings.formSelector + ' .formCloseFullConfirm');

      fullConfirmCheckEl.on("change",fullConfirmCheckChange);
      showFullConfirmEl.on("click",showFullConfirm);
      closeFullConfirmEl.on("click",closeFullConfirm);
      
      if (settings.fieldsForValidation==null) {
        settings.fieldsForValidation = {
          name:true,
          email:true,
          phone:true,
          confirm:true,
        }
      }
    }

    function showFullConfirm() {
      fullConfirmEL.addClass('visible');
      fullConfirmCheckEl.prop("checked",inputConfirmEl.prop("checked"));
      return false;
    }

    function closeFullConfirm() {
      fullConfirmEL.removeClass('visible');
    }

    function fullConfirmCheckChange() {
      if ($(this).prop("checked")) {
        closeFullConfirm();
      }
      inputConfirmEl.prop("checked",$(this).prop("checked"));
    }

    function validate() {
      validateStatus = {
        name:true,
        email:true,
        phone:true,
        text:true,
        confirm:true,
      }

      values = {
        name:inputNameEl.val(),
        email:inputEmailEl.val(),
        phone:inputPhoneEl.val(),
        text:inputTextEl.val(),
        confirm:inputConfirmEl.prop("checked"),        
      }

      console.log(values);
      
      var validateTotal = true;

      
      if (settings.fieldsForValidation.name && values.name=="") {
        validateStatus.name =  false;
        validateTotal = false;
      }
      if (settings.fieldsForValidation.email &&  values.email=="") {
        validateStatus.email =  false;
        validateTotal = false;
      }
      
      if (settings.fieldsForValidation.phone &&  (values.phone=="" || !inputPhoneEl.inputmask("isComplete"))) {
        validateStatus.phone =  false;
        validateTotal = false;
      }

      if (settings.fieldsForValidation.text &&  values.text=="") {
        validateStatus.text =  false;
        validateTotal = false;
      }

      if (settings.fieldsForValidation.confirm && validateTotal==true && values.confirm==false) {
        validateStatus.confirm = false;
        validateTotal = false;
      }

      updateValidate(validateStatus);

      if (validateTotal) {
        formEl.addClass('custom-form-sending');
        if (productNameEl.length!=0) {
          values.productName = productNameEl.text();
        }
        send(values);
      }   
    } 

    function updateValidate(status) {

      if (!status.name) {
        wrapNameEl.addClass('custom-form-error');
      }
      else {
        wrapNameEl.removeClass('custom-form-error');
      }
      if (!status.email) {
        wrapEmailEl.addClass('custom-form-error');
      }
      else {
        wrapEmailEl.removeClass('custom-form-error');
      }
      if (!status.phone) {
        wrapPhoneEl.addClass('custom-form-error');
      }
      else {
        wrapPhoneEl.removeClass('custom-form-error');
      }
      if (!status.text) {
        wrapTextEl.addClass('custom-form-error');
      }
      else {
        wrapTextEl.removeClass('custom-form-error');
      }

      if (!status.confirm) {
        wrapConfirmEl.addClass('custom-form-error');
      }
      else {
        wrapConfirmEl.removeClass('custom-form-error');
      }
    }

    function send(values) {    
      var data = generateData(values);
      values.date = data;
      $.ajax({
        type: "POST",
        url: settings.apiUrl,
        data: values,
        success: sendCallback,
        dataType: "json"
      });
    }

    function sendCallback(data) {
      formEl.removeClass('custom-form-sending');
      if (data.status) {
        formEl.addClass('custom-form-success');
      }
      else {
        formEl.addClass('custom-form-fail');
      }
    }

    function generateData(values) {
      //var result =  17*(1373 + values.name.length + values.email.length + values.phone.length);
      var date = new Date(); 
      var result = 4721*date.getSeconds();
      return result;
    }

    function clear() {
      inputNameEl.val('');
      inputEmailEl.val('');
      inputPhoneEl.val('');
      validateStatus = {
        name:true,
        email:true,
        phone:true,
        text:true,
        confirm:true,
      }
      updateValidate(validateStatus);
      formEl.removeClass('custom-form-sending');
      formEl.removeClass('custom-form-success');
      formEl.removeClass('custom-form-fail');
    }
  }

  window.CustomForm = CustomForm;
})(jQuery);
$(document).ready(function() {
   window.mainForm = new window.CustomForm({
    formSelector:'.main-form',
    apiUrl:'/form/send',
    fieldsForValidation:{
      name:true,
      phone:true,
      email:true,
      text:true,
      confirm:true,
    }
  });
});
$(document).ready(function() {
   window.orderPopupForm = new window.CustomForm({
    formSelector:'.order-popup',
    apiUrl:'/form/send',
    fieldsForValidation:{
      name:true,
      phone:true,
      email:true,
      confirm:true,
    }
  });
   window.orderPopup = new OrderPopup;
});

function OrderPopup() {
	var popup = $('.order-popup');
	var bg = $('.order-popup__bg');
	var prodNameEl = $('.order-popup__prod-name');
	var prodImgEl = $('.order-popup__prod-img');
	
	this.show = show;
	this.close = close;
	this.send = send;

	function show() {
		window.orderPopupForm.clear();
		var top = $(document).scrollTop() + 30;
		popup.css('top',top);
		popup.addClass('equip-popup_visible');
		bg.addClass('equip-popup__bg_visible');
		//$('body').css('overflow','hidden');
	}

	function close() {
		popup.removeClass('equip-popup_visible');
		bg.removeClass('equip-popup__bg_visible');
		$('body').css('overflow','visible');
	}

	function send() {
		close();
	}
}