$(document).ready(function() {
   window.mainForm = new window.CustomForm({
    formSelector:'.main-form',
    apiUrl:'/form/send',
    fieldsForValidation:{
      name:true,
      phone:true,
      email:true,
      text:true,
      confirm:true,
    }
  });
});