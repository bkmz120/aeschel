;(function() {
    var fixedMenuStatus = "top";
    var menuEl = $('.text-page-menu-list'),
        menuWrapEl = $('.text-page_menu'),
        textEl = $('.text-page_content');

    var bottomTrigger = $('.text-page_bottom').offset().top;
    var menuElHeight = menuEl.height();
    menuWrapEl.height(textEl.height());

    $(window).scroll(function() {
        var pos = $(this).scrollTop();
        if (pos<82) {
        	if (fixedMenuStatus!="top")	{
                menuEl.removeClass('text-page-menu-list__bottom');
        		menuEl.removeClass('text-page-menu-list__middle');
            	fixedMenuStatus="top";
        	}  	
        }
        else if (pos > 82 && pos < (bottomTrigger - menuElHeight - 296)) {
        	if (fixedMenuStatus!="middle")	{
    	        menuEl.removeClass('text-page-menu-list__bottom');
                menuEl.addClass('text-page-menu-list__middle');
    	        fixedMenuStatus="middle";
    	    }
        }
        else {
            if (fixedMenuStatus!="bottom") {
                menuEl.addClass('text-page-menu-list__bottom');
                menuEl.removeClass('text-page-menu-list__middle');
                fixedMenuStatus="bottom";
            }
        }     
    });
}());